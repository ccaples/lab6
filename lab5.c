//
/*****************
Chloe Caples
ccaples
Lab 5
Lab Section: 4
Anurata Hridi	
******************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  Card deck[52];
  for (int i = 0; i <= 52; i++)
  {
    	deck[i].suit = Suit(i%4);
        deck[i].value = ((i%13) + 2);
  }
  
  
  
    
  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   random_shuffle(deck, &deck[52], myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
   Card hand[5];
   for (int i = 0; i<=4; i++) 
   {
	hand[i] = deck[i];
   }

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
   sort(hand, hand+5, suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */

   for (int i = 0; i<=4; i++)
   {
	cout << setw(10) << get_card_name(hand[i]) << " of " << get_suit_code(hand[i]) << endl;
   	
   }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
	//compare left and right hands
	//if left suit is less than right suit return true	
	if (lhs.suit < rhs.suit)
	{
		return true;
	}
	//if left suit is greater than right suit compare values
	else if (lhs.suit == rhs.suit)
	{
	     //if left value is less than right value return true
	     if (lhs.value < rhs.value)
	     {   
		return true;
	     }
	     else
	     {
		return false;
	     }
	}
	else
	{
	     //if left value is greater than right value then return false
		return false;
        }
	   
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT
  switch (c.value) {
    case 2:        return "2";
    case 3:        return "3";
    case 4:        return "4";
    case 5:        return "5";
    case 6:        return "6";
    case 7:        return "7";
    case 8:        return "8";
    case 9:        return "9";
    case 10:       return "10";
    case 11:       return "Jack";
    case 12:       return "Queen";
    case 13:       return "King";
    case 14:       return "Ace";
    default:       return "";	
  }
}
